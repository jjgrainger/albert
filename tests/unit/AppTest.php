<?php

class AppTest extends \PHPUnit_Framework_TestCase
{
    public function test_container_is_instance_of_pimple()
    {
        $app = new Albert\App;

        $this->assertInstanceOf(Pimple\Container::class, $app->getContainer());
    }

    public function test_can_retrieve_items_from_container()
    {
        $app = new Albert\App;

        $container = $app->getContainer();

        $container['config'] = function($c) {
            return ['name' => 'test'];
        };

        $this->assertEquals($app->config, ['name' => 'test']);
        $this->assertEquals($app->config['name'], 'test');
    }

    public function test_return_null_if_item_does_not_exist_in_container()
    {
        $app = new Albert\App;

        $this->assertNull($app->config);
    }
}
