<?php

class ParameterBagTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Instantiation methods
     */

    public function test_bag_is_empty_when_instantiated()
    {
        $bag = new Albert\ParameterBag;

        $this->assertEmpty($bag->all());
        $this->assertEquals($bag->count(), 0);
    }

    public function test_items_returned_match_items_instantiated_with()
    {
        $bag = new  Albert\ParameterBag([
            'name' => 'Joe',
            'email' => 'admin@email.com'
        ]);

        $items = $bag->all();

        $this->assertEquals($items['name'], 'Joe');
        $this->assertEquals($items['email'], 'admin@email.com');
    }

    /**
     * Get methods
     */

    public function test_get_returns_null_if_not_set()
    {
        $bag = new Albert\ParameterBag;

        $this->assertNull($bag->get('name'));
    }

    public function test_get_returns_default_if_not_set()
    {
        $bag = new Albert\ParameterBag;

        $this->assertEquals($bag->get('name', 'Joe'), 'Joe');
    }

    public function test_get_returns_item_if_set()
    {
        $bag = new  Albert\ParameterBag([
            'name' => 'Joe',
            'email' => 'admin@email.com'
        ]);

        $this->assertEquals($bag->get('name'), 'Joe');
    }
}
