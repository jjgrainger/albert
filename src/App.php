<?php

namespace Albert;

use Pimple\Container;

/**
 * Albert Application
 */
class App
{

    /**
     * The Application DI Container
     * @var Pimple\Container Instance
     */
    private $container;

    /**
     * Create the Application
     * @param Container|null $container
     */
    function __construct(Container $container = null)
    {
        if (isset($container)) {
            $this->container = $container;
        } else {
            $this->container = new Container;
        }
    }

    /**
     * Return the Application container
     * @return $container
     */
    function getContainer()
    {
        return $this->container;
    }

    /**
     * Get container instances through Application object
     * @return mixed
     */
    function __get($name)
    {
        if (isset($this->container[$name])) {
            return $this->container[$name];
        }

        // should throw exception
        return null;
    }

}
