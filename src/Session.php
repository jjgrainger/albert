<?php
namespace Albert;


Class Session {

  // want to look into session storage handlers
  public function __construct()
  {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
  }

  public function set($key, $value) {
    return $_SESSION[$key] = $value;
  }

  public function has($name) {
    return (isset($_SESSION[$name])) ? true : false;
  }

  public function get($name, $default = null) {
    if ($this->has($name)) {
      return $_SESSION[$name];
    }
    return $default;
  }

  public function delete($name) {
    if ($this->has($name)) {
      unset($_SESSION[$name]);
    }
  }

  public function all() {
    return $_SESSION;
  }

  public function flash($key, $value = '') {

    if ($this->has('flash.' . $key)) {

      $message = $this->get('flash.' . $key);
      $this->delete('flash.' . $key);
      return $message;

    } else {
      $this->set('flash.' . $key, $value);
    }
  }

  public function regenerate() {
    session_regenerate_id();
  }
}
