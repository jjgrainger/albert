<?php

namespace Albert;

class ParameterBag
{

    /**
     * Attribute store
     * @var array
     */
    private $attributes;

    /**
     * Create parameter bag
     * @param array $attributes
     */
    function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * Return all attributes in bag
     * @return array
     */
    function all()
    {
        return $this->attributes;
    }

    /**
     * Return number of items in the bag
     * @return int
     */
    function count()
    {
        return count($this->attributes);
    }

    /**
     * Get an attribute from the bag
     * @param  string $key
     * @param  mixed $default
     */
    function get($key, $default = null)
    {
        if ($this->has($key)) {
            return $this->attributes[$key];
        }

        return $default;
    }

    /**
     * See if key exists in bag
     * @param  string $key
     * @return boolean
     */
    function has($key)
    {
        return isset($this->attributes[$key]);
    }

}
